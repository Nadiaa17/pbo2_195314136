
import java.util.Scanner;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Asus
 */
public class MahasiswaMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner (System.in);
        Mahasiswa mhs = new Mahasiswa ();
        String nama,nim;
        nama = JOptionPane.showInputDialog(null,"Masukkan Nama Mahasiswa:");
        nim = JOptionPane.showInputDialog(null,"Masukkan Nim Mahasiswa:");
        mhs.setNama(nama);
        mhs.setNim(nim);
        System.out.println("Nama Mahasiswa:"+mhs.getNama());
        System.out.println("NIM:"+mhs.getNim());
        
        Handphone hp = new Handphone();
        hp.setMerk("Asus");
        hp.setWarna("warna");
        hp.setProsesor("Intel Atom Z3560");
        hp.setRam("2GB");
        hp.setRom("16GB");
        hp.setKonektivitas("GSM,HSPA,LTE");
        
         Tablet tb = new Tablet();
        tb.setMerk("Samsung");
        tb.setWarna("Hitam");
        tb.setProsesor("Marvel PXA1088");
        tb.setRam("1,5GB");
        tb.setRom("8GB");
        tb.setKonektivitas("Wifi");
        
         Laptop lp = new Laptop();
        lp.setMerk("Lenovo ThinkPad T400");
        lp.setWarna("Hitam");
        lp.setProsesor("Core 2 Duo 2.53 GHz");
        lp.setRam("2GB");
        lp.setHardisk("160GB");
        lp.setJumlahUSB(3);
        
        System.out.println("   Handphone                              Tablet                     Laptop");
        System.out.println("Merk:"+hp.getMerk()+             "Merk:"+tb.getMerk()+         "Merk:"+lp.getMerk());
        System.out.println("Warna:"+hp.getWarna()+             "Warna:"+tb.getWarna()+         "Warna:"+lp.getWarna());
        System.out.println("Prosesor:"+hp.getProsesor()+             "Prosesor:"+tb.getProsesor()+         "Prosesor:"+lp.getProsesor());
        System.out.println("Ram:"+hp.getRam()+             "Ram:"+tb.getRam()+              "Ram:"+lp.getRam());
        System.out.println("Rom:"+hp.getRom()+             "Rom:"+tb.getRom()+         "Hardisk:"+lp.getHardisk());
        System.out.println("Konektivitas:"+hp.getKonektivitas()+     "Konektivitas:"+tb.getKonektivitas()+         "Jumlah port USB:"+lp.getJumlahUSB());
    }
    
}
