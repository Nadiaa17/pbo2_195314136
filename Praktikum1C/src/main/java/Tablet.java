/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Asus
 */
public class Tablet extends Gadget  {
    private  String rom;
    private String konektivitas;

    public String getRom() {
        return rom;
    }

    public void setRom(String rom) {
        this.rom = rom;
    }

    public String getKonektivitas() {
        return konektivitas;
    }

    public void setKonektivitas(String konektivitas) {
        this.konektivitas = konektivitas;
    }
    
}

