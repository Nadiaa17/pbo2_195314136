package model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Asus
 */
public abstract class Penduduk {
    protected String nama;
    protected String tempatTanggalLahir;

    public Penduduk(String nama, String tempatTanggalLahir) {
        this.nama = nama;
        this.tempatTanggalLahir = tempatTanggalLahir;
    }
    public Penduduk() {
    }
    public void setNama(String nama) {
        this.nama = nama;
    }
    public String getNama() {
        return nama;
    }
    public void setTempatTanggalLahir(String tempatTanggalLahir) {
        this.tempatTanggalLahir = tempatTanggalLahir;
    }
    public String getTempatTanggalLahir() {
        return tempatTanggalLahir;
    }
    abstract double hitungIuran();
}
